var services = angular.module('services', ['models']);

var fs = require('fs');

services.factory('MemoryManager', [function() {
    var MemoryManager = {
        _saveName: 'default_save',
        _data: {
            ids: new Object(),
            models: new Object(),
            objects: new Object()
        },
        _save: function() {
            fs.writeFileSync('./saves/' + this._saveName + '.json', JSON.stringify(this._data));
        },
        _load: function(name) {
            if (fs.existsSync('./saves/' + name + '.json')) {
                this._saveName = name;
                this._data = JSON.parse(fs.readFileSync('./saves/' + name + '.json', 'utf-8'));
            } else {
                return 'Save "' + name + '" not found !';
            }
        },
        registerId: function(name, value) {
            this._data.ids[name] = value;
        },
        registerModel: function(model) {
            this._data.models[model.id] = model;
        },
        registerDraftObject: function(draftObject) {
            if (undefined == this._data.objects[draftObject._modelId]) {
                this._data.objects[draftObject._modelId] = new Object();
            }
            this._data.objects[draftObject._modelId][draftObject.id] = draftObject;
        },
        getId: function(name) {
            if (this._data.ids[name] != undefined) {
                return this._data.ids[name];
            } else {
                return 0;
            }
        },
        getModel: function(id) {
            if (this._data.models[id] != undefined) {
                return this._data.models[id];
            }
        },
        getModels: function() {
            return this._data.models;
        },
        getDraftObject: function(modelId, id) {
            if (this._data.objects[modelId][id] != undefined) {
                return this._data.objects[modelId][id];
            }
        },
        getDraftObjects: function() {
            return this._data.objects;
        },
        removeId: function(name) {
            delete this._data.ids[name];
        },
        removeModel: function(id) {
            delete this._data.models[id];
        },
        removeDraftObject: function(modelId, id) {
            delete this._data.objects[modelId][id];
        }
    };

    return MemoryManager;
}]);

services.factory('ModelsManager', ['MemoryManager', 'Model', function(MemoryManager, Model) {
    var ModelsManager = {
        _pull: new Object(),
        _initFromMemory: function() {
            if (this._pull.length > 0)
                return;

            var data_pull = MemoryManager.getModels();

            for (var i in data_pull) {
                this._pull[data_pull[i].id] = new Model(data_pull[i]);
            }
        },
        _saveToMemory: function() {
            for (var i = 0, len = this._pull.length; i < len; i++) {
                MemoryManager.registerModel(this._pull[i]);
            }
        },
        createModel: function(data) {
            if (!data) {
                data = new Object();
            }

            data.id = MemoryManager.getId('models');
            MemoryManager.registerId('models', (data.id + 1));

            this._pull[data.id] = new Model(data);
            this._saveToMemory();

            return this._pull[data.id];
        },
        getModel: function(id) {
            this._initFromMemory();
            if (this._pull[id]) {
                return this._pull[id];
            }
        },
        getModels: function() {
            this._initFromMemory();
            return this._pull;
        },
        removeModel: function(id) {
            if (this._pull[id]) {
                delete this._pull[id];
                MemoryManager.removeModel(id);
            }
        }
    };

    return ModelsManager;
}]);

services.factory('DraftObjectsManager', ['MemoryManager', 'DraftObject', function(MemoryManager, DraftObject) {
    var DraftObjectsManager = {
        _pull: new Object(),
        _initFromMemory: function() {
            if (this._pull.length > 0)
                return;

            var data_pull = MemoryManager.getDraftObjects();

            for (var i in data_pull) {
                this._pull[i] = new Object();
                for (var j in data_pull[i]) {
                    this._pull[i][j] = new DraftObject();
                    this._pull[i][j].restore(data_pull[i][j]);
                }
            }
        },
        _saveToMemory: function() {
            for (var i = 0, len = this._pull.length; i < len; i++) {
                for (var j = 0, j_len = this._pull[i].length; j < j_len; j++) {
                    MemoryManager.registerDraftObject(this._pull[i][j]);
                }
            }
        },
        createObject: function(model) {
            var id = MemoryManager.getId('objects');
            MemoryManager.registerId('objects', (id + 1));
            this._pull[model.id][id] = new DraftObject(model, {id: id});

            this._saveToMemory();

            return this._pull[model.id][id];
        },
        getObject: function(modelId, id) {
            this._initFromMemory();
            if (this._pull[modelId] && this._pull[modelId][id]) {
                return this._pull[modelId][id];
            }
        },
        getObjects: function(modelId) {
            this._initFromMemory();
            if (this._pull[modelId]) {
                return this._pull[modelId];
            }
        },
        removeObject: function(modelId, id) {
            if (this._pull[modelId] && this._pull[modelId][id]) {
                delete this._pull[modelId][id];
                MemoryManager.removeDraftObject(modelId, id);
            }
        }
    };

    return DraftObjectsManager;
}]);