var controllers = angular.module('controllers', ['services']);

controllers.controller('TitleController', ['$scope', function($scope) {
    $scope.goTo = function(location) {
        switch (location) {
            case 'new':
                window.location.assign('#/project');
                break;
            case 'load':
                window.location.assign('#/load/project');
                break;
            default:
                window.close();
        }
    }
}]);

controllers.controller('ModelsListController', ['ModelsManager', '$scope', function(ModelsManager, $scope) {
    $scope.models = ModelsManager.getModels();
    $scope.newName = '';

    $scope.addModel = function() {
        if ($scope.newName.length) {
            $scope.models.push(ModelsManager.createModel({name: $scope.newName}));
            $scope.newName = '';
        } else {
            $scope.models.push(ModelsManager.createModel());
        }
    }

    $scope.editModel = function(id) {
        $scope.$parent.$parent.changeCenterView('partials/model_edit.html', {
            id: id
        });
    }
}]);

controllers.controller('ModelEditController', ['ModelsManager', '$scope', function(ModelsManager, $scope) {
    $scope.model = ModelsManager.getModel($scope.$parent.$parent.view.params.center.id);

    $scope.addField = function() {
        $scope.model.fields.push({
            name: 'New one',
            type: 'text'
        });
    }
}]);

controllers.controller('ProjectController', ['$scope', function($scope) {
    $scope.view = {
        params: new Object(),
        left: 'partials/project_views/models_list.html'
    };

    $scope.changeCenterView = function(page, params) {
        $scope.view.params.center = params;
        $scope.view.center = page;
    }
}]);