var models = angular.module('models', []);

models.factory('Model', [function() {
    function Model() {
        var self = this;

        self.id = -1;
        self.name = 'Object';
        self.fields = new Array();

        var values = arguments[0];
        if (values) {
            angular.extend(self, values);
        }
    }

    return Model;
}]);

models.factory('DraftObject', ['Model', function() {
    function DraftObject(model) {
        var self = this;
        self._modelId = -1;

        self.initFromModel = function(model, data) {
            for (var i in model.fields) {
                self[model.fields[i].name] = model.fields[i].default;
                if (data && data[model.fields[i].name]) {
                    self[model.fields[i].name] = data[model.fields[i].name];
                }
            }
            self._modelId = model.id;
        }

        self.restore = function(data) {
            for (var i in data) {
                self[i] = data[i];
            }
        }

        if (model) {
            var data = arguments[1];
            self.initFromModel(model, data);
        }
    }

    return DraftObject;
}]);