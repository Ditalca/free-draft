var app = angular.module('routing', ['ngRoute', 'controllers']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'partials/title.html',
            controller: 'TitleController'
        })
        .when('/project', {
            templateUrl: 'partials/project.html',
            controller: 'ProjectController'
        })

}]);